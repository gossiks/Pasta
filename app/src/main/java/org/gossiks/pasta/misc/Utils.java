package org.gossiks.pasta.misc;

import android.util.Log;

import org.gossiks.pasta.BuildConfig;

/**
 * Created by gossiks on 29.01.17.
 */

public class Utils {
    public static void log(String log) {
        if (BuildConfig.DEBUG) {
            Log.d("pastalog", log);
        }
    }

    public static void trace(Exception e) {
        if (BuildConfig.DEBUG && e != null) {
            e.printStackTrace();
        }
    }
}
