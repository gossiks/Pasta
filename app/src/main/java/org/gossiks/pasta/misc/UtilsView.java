package org.gossiks.pasta.misc;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by gossiks on 25.02.17.
 */

public class UtilsView {

    /**
     * Lightweight replacement for enum state constants.
     * Use for any kind of View state management.
     */
    @Retention(SOURCE)
    @IntDef({VISIBLE_STATE, LOADING_STATE, ERROR_STATE})
    public @interface STATE_VIEW {
    }

    public static final int VISIBLE_STATE = 0;
    public static final int LOADING_STATE = 1;
    public static final int ERROR_STATE = 2;

}
