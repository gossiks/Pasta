package org.gossiks.pasta.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by gossiks on 25.02.17.
 */

public class PastaString {

    String[] mWords;
    private PastaFormatterPack mFormatterPack;
    private HandledString mHandledString;
    private StringBuilder mSetWordsStringBuilder;

    public PastaString setWords(String words) {
        mWords = words.split(" ");
        return this;
    }

    public String getProcessedString() {
        if (mHandledString != null) {
            return mHandledString.getString();
        }
        return handleString().getString();
    }

    public HandledString handleString() {
        StringBuilder stringBuilder = new StringBuilder();
        boolean wasModified = false;
        for (String word : mWords) {
            String newWord = String.valueOf(word);

            for (PastaFormatter formatter : getFormatterPack().getFormatters()) {
                if (formatter.getPatternToMatch().matcher(word).matches()) {
                    newWord = formatter.getModifiedString(newWord);
                    wasModified = true;
                }
            }

            stringBuilder.append(newWord).append(" ");
        }
        mHandledString = new HandledString(wasModified, stringBuilder.toString());
        return mHandledString;
    }

    public boolean needRefresh() {
        return handleString().isWasModified();
    }

    private static String getCombinedAllWith(String combineString, String word) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {//TODO: handle non-alphabetic characters properly
            builder.append(word.charAt(i)).append(combineString);
        }
        return builder.toString();
    }

    public PastaString setWords(List<CharSequence> text) {
        mSetWordsStringBuilder = new StringBuilder();
        for (CharSequence seq : text) {
            mSetWordsStringBuilder.append(seq.toString());
        }
        return setWords(mSetWordsStringBuilder.toString());
    }

    private class PastaFormatter {

        Pattern mPattern;

        Pattern getPatternToMatch() {
            if (mPattern == null) {
                mPattern = createPatternToMatch();
            }
            return mPattern;
        }

        Pattern createPatternToMatch() {
            return Pattern.compile("");
        }

        String getTrimmedString(String stringToTrim) {
            return stringToTrim;
        }

        String getModifierString() {
            return "";
        }

        String getModifiedString(String stringToModify) {
            return getCombinedAllWith(getModifierString(), getTrimmedString(stringToModify));
        }
    }

    private PastaFormatterPack getFormatterPack() {
        if (mFormatterPack == null) {
            mFormatterPack = new PastaFormatterPack().addFormatter(
                    new PastaFormatterStrikethrough()).addFormatter(new PastaFormatterUnderline());
        }
        return mFormatterPack;
    }

    private class PastaFormatterStrikethrough extends PastaFormatter {


        @Override
        public Pattern createPatternToMatch() {
            return Pattern.compile(
                    "(^|\\s)\\-{1}([\\w\\d ]+)\\-{1}($|\\s)"); // (^|\s)\~{2}([\w\d ]+)\~{2}
            // ($|\s)   //"a\u0336"
        }

        @Override
        public String getTrimmedString(String stringToTrim) {
            return stringToTrim.replaceAll("^\\-{1}+", "").replaceAll("\\-{1}+$", "");
        }

        @Override
        String getModifierString() {
            return "\u0336";
        }
    }

    private class PastaFormatterUnderline extends PastaFormatter {

        @Override
        public Pattern createPatternToMatch() {
            return Pattern.compile(
                    "(^|\\s)\\_{1}([\\w\\d ]+)\\_{1}($|\\s)"); // (^|\s)\*{1}([\w\d ]+)\*{1}($|\s)
        }

        @Override
        public String getTrimmedString(String stringToTrim) {
            return stringToTrim.replaceAll("^\\_{1}+", "").replaceAll("\\_{1}+$", "");
        }

        @Override
        String getModifierString() {
            return "\u035F"; // Because "\u0332" renders on different heights for different fonts.
        }
    }


    private class PastaFormatterPack {
        ArrayList<PastaFormatter> mFormatters;

        public ArrayList<PastaFormatter> getFormatters() {
            return mFormatters;
        }

        public PastaFormatterPack addFormatter(PastaFormatter formatter) {
            getFormattersSafe().add(formatter);
            return this;
        }

        private ArrayList<PastaFormatter> getFormattersSafe() {
            if (mFormatters == null) {
                mFormatters = new ArrayList<>();
            }
            return mFormatters;
        }
    }

    private class HandledString {
        String mString;
        boolean mWasModified;

        public HandledString(boolean wasModified, String string) {
            mWasModified = wasModified;
            mString = string;
        }

        public String getString() {
            return mString;
        }

        public boolean isWasModified() {
            return mWasModified;
        }
    }
}
