package org.gossiks.pasta.service;

import android.accessibilityservice.AccessibilityService;
import android.os.Bundle;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.EditText;

import org.gossiks.pasta.misc.Utils;
import org.gossiks.pasta.parse.PastaString;

/**
 * Created by gossiks on 29.01.17.
 */

public class PastaService extends AccessibilityService {
    PastaString mPastaString;

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        final int eventType = event.getEventType();
        if (eventType != AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED) {
            return; // TODO: is it really possible? Check it.
        }

        AccessibilityNodeInfo source = event.getSource();
        boolean isEditText = false;

        try {
            isEditText = isEditText(event);
        } catch (ClassNotFoundException e) {
            Utils.trace(e);
        }

        if (isEditText) {
            if (getPastaString().setWords(event.getText()).needRefresh()) {
                Bundle arguments = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo
                                .ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE,
                        getPastaString().getProcessedString());
                source.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
            }
        }
    }

    private boolean isEditText(AccessibilityEvent event) throws ClassNotFoundException {
        Class inputClass = Class.forName(String.valueOf(event.getClassName()));
        return EditText.class.isAssignableFrom(inputClass);
    }

    @Override
    public void onInterrupt() {
        Utils.log("Interrupted");
    }

    public PastaString getPastaString() {
        if (mPastaString == null) {
            mPastaString = new PastaString();
        }
        return mPastaString;
    }
}
