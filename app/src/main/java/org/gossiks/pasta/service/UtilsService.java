package org.gossiks.pasta.service;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.text.TextUtils;

import org.gossiks.pasta.misc.Utils;

/**
 * Created by gossiks on 24.02.17.
 */

public class UtilsService {

    public static Intent getSettingsIntent() {
        return new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
    }


    /**
     * Check if service is enabled
     * Code snippet from: http://stackoverflow.com/a/18095283
     *
     * @param mContext
     * @param serviceClass
     * @return
     */
    public static boolean isAccessibilitySettingsOn(Context mContext, Class serviceClass) {
        int accessibilityEnabled = 0;
        final String service = mContext.getPackageName() + "/" + serviceClass.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Utils.log("accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            Utils.log("Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            Utils.log("***ACCESSIBILITY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    Utils.log("-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        Utils.log("We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            Utils.log("***ACCESSIBILITY IS DISABLED***");
        }

        return false;
    }

    public static void setServiceOnline(boolean online, Context context) {
        Intent i = new Intent(context, PastaService.class);

        if (online) {
            context.startService(i);
        } else {
            context.stopService(i);
        }
    }
}
