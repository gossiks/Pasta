package org.gossiks.pasta.settings;

import static org.gossiks.pasta.misc.UtilsView.LOADING_STATE;
import static org.gossiks.pasta.misc.UtilsView.VISIBLE_STATE;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.gossiks.pasta.R;
import org.gossiks.pasta.misc.UtilsView;
import org.gossiks.pasta.service.PastaService;
import org.gossiks.pasta.service.UtilsService;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by gossiks on 24.02.17.
 */

public class SettingsFragment extends Fragment {

    private ProgressBar mServiceIsRunningProgressBar;
    private CheckBox mServiceIsRunningCheckBox;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container);
        ((TextView) view.findViewById(R.id.description)).setText(Html.fromHtml(
                getString(R.string.description_settings)
                        + getString(R.string.description_enable_service)));
        mServiceIsRunningProgressBar = (ProgressBar) view.findViewById(
                R.id.pasta_is_running_progress);
        mServiceIsRunningCheckBox = ((CheckBox) view.findViewById(R.id.pasta_is_running_checkbox));
        getServiceState(getActivity()).subscribe(aBoolean -> {
            mServiceIsRunningCheckBox.setChecked(aBoolean);
            mServiceIsRunningCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                setState(LOADING_STATE);
                getServiceState(buttonView.getContext())
                        .subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()).subscribe(launched -> {
                    if (isChecked && !launched) {
                        AlertDialog alertDialog = new AlertDialog.Builder(buttonView.getContext())
                                .setMessage(R.string.enable_service_to_settings)
                                .setPositiveButton(R.string.switch_service_state_button,
                                        (dialog, which) -> getActivity()
                                                .startActivity(UtilsService.getSettingsIntent()))
                                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                                    dialog.dismiss();
                                }).setOnDismissListener(
                                        dialog -> mServiceIsRunningCheckBox.setChecked(
                                                false)).create();
                        alertDialog.show();
                    } else if (!isChecked && launched) {
                        AlertDialog alertDialog = new AlertDialog.Builder(buttonView.getContext())
                                .setMessage(R.string.disable_service_to_settings)
                                .setPositiveButton(R.string.switch_service_state_button,
                                        (dialog, which) -> getActivity()
                                                .startActivity(UtilsService.getSettingsIntent()))
                                .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                                    dialog.dismiss();
                                }).setOnDismissListener(
                                        dialog -> mServiceIsRunningCheckBox.setChecked(
                                                true)).create();
                        alertDialog.show();
                    }
                    setState(VISIBLE_STATE);
                });
            });
        });


        return view;
    }

    private void setState(
            @UtilsView.STATE_VIEW int state) { // TODO: redo to many settings and buttons
        switch (state) {
            case VISIBLE_STATE:
                mServiceIsRunningCheckBox.setVisibility(View.VISIBLE);
                mServiceIsRunningProgressBar.setVisibility(View.GONE);
                break;
            case LOADING_STATE:
                mServiceIsRunningCheckBox.setVisibility(View.GONE);
                mServiceIsRunningProgressBar.setVisibility(View.VISIBLE);
                break;
        }

    }

    private Flowable<Boolean> getServiceState(Context context) {
        return Flowable.just(
                UtilsService.isAccessibilitySettingsOn(context, PastaService.class));
    }
}
